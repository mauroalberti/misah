module constants

    implicit none

    ! constants declaration
    real (kind=r8b) :: pi ! pi radians
    real (kind=r8b) :: r2d ! conversion from radians to degrees
    real (kind=r8b) :: d2r ! conversion from degrees to radians


    contains


end module constants